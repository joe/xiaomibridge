bottle==0.12.17
certifi==2019.6.16
chardet==3.0.4
Click==7.0
colorama==0.4.1
colorlog==4.0.2
ecdsa==0.13.2
esphome==1.13.6
esptool==2.7
idna==2.8
ifaddr==0.1.6
paho-mqtt==1.4.0
pkg-resources==0.0.0
platformio==4.0.3
protobuf==3.7.1
pyaes==1.6.1
pyserial==3.4
pytz==2019.2
PyYAML==5.1.2
requests==2.22.0
semantic-version==2.8.1
six==1.12.0
tabulate==0.8.3
tornado==5.1.1
tzlocal==2.0.0
urllib3==1.25.3
voluptuous==0.11.5



1:
voluptuous==0.11.6
voluptuous==0.11.7

2:
ESP32dev Board - Compile fails with Error in AsyncTCP / AsyncMqttClient #476 
https://github.com/esphome/issues/issues/476


